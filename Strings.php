<?php

class Strings {
    const DEFAULT_ENCRYPT_SALT = '2cX@m0Ce';
    const OPEN_SSL_CONSTANT_IV = '9HihO52?I**#4@p78@uWRIbrIG!_6o-&-_aF@1LWlxa1RI0LX_y6sT1PhUfr59!Z';


    public static function html_encode($text){
        return htmlentities($text,ENT_QUOTES,'UTF-8');
    }


    public static function len($str){
        return mb_strlen($str);
    }

    public static function jdie($array){
        die(
            json_encode($array)
        );
    }

    public static function count_words($string) {
        $string = strip_tags($string);
        // Return the number of words in a string.
        $string = str_replace("&#039;", "'", $string);
        $t = array(' ', "\t", '=', '+', '-', '*', '/', '\\', ',', '.', ';', ':', '[', ']', '{', '}', '(', ')', '<', '>', '&', '%', '$', '@', '#', '^', '!', '?', '~'); // separators
        $string = str_replace($t, " ", $string);
        $string = trim(preg_replace("/\s+/", " ", $string));
        $num = 0;
        if (self::length($string)>0) {
            $word_array = explode(" ", $string);
            $num = count($word_array);
        }
        return $num;
    }
    
    public static function substr($str, $start, $offset){
        return mb_substr($str, $start, $offset, "UTF-8");
    }


    public static function length($str){
        return self::strlen($str);
    }
    public static function strlen($str){
        $count = 0;
    
        for($i = 0; $i < strlen($str); $i++)
        {
            $value = ord($str[$i]);
            if($value > 127)
                {
                if($value >= 192 && $value <= 223)
                $i++;
                elseif($value >= 224 && $value <= 239)
                $i = $i + 2;
                elseif($value >= 240 && $value <= 247)
                $i = $i + 3;
                else
                die('Not a UTF-8 compatible string');
                }
    
            $count++;
        }
    
        return $count;
    }

    public static function removeNonNumeric($string) {
        return preg_replace("/[^0-9]/", "", $string );
    }


    private static function alphaID($in, $to_num = false, $pad_up = false, $passKey = null) {
        //NEVER ADD '-' AS to index for it's used as a seperator in other places (like the forum)
      $index = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
      if ($passKey !== null) {
          // Although this function's purpose is to just make the
          // ID short - and not so much secure,
          // with this patch by Simon Franz (http://blog.snaky.org/)
          // you can optionally supply a password to make it harder
          // to calculate the corresponding numeric ID
    
          for ($n = 0; $n<strlen($index); $n++) {
              $i[] = substr( $index,$n ,1);
          }
    
          $passhash = hash('sha256',$passKey);
          $passhash = (strlen($passhash) < strlen($index))
              ? hash('sha512',$passKey)
              : $passhash;
    
          for ($n=0; $n < strlen($index); $n++) {
              $p[] =  substr($passhash, $n ,1);
          }
    
          array_multisort($p,  SORT_DESC, $i);
          $index = implode($i);
      }
    
      $base  = strlen($index);
    
      if ($to_num) {
          // Digital number  <<--  alphabet letter code
          $in  = strrev($in);
          $out = 0;
          $len = strlen($in) - 1;
          for ($t = 0; $t <= $len; $t++) {
              $bcpow = bcpow($base, $len - $t);
              $out   = $out + strpos($index, substr($in, $t, 1)) * $bcpow;
          }
    
          if (is_numeric($pad_up)) {
              $pad_up--;
              if ($pad_up > 0) {
                  $out -= pow($base, $pad_up);
              }
          }
          $out = sprintf('%F', $out);
          $out = substr($out, 0, strpos($out, '.'));
      } else {
          // Digital number  -->>  alphabet letter code
          if (is_numeric($pad_up)) {
              $pad_up--;
              if ($pad_up > 0) {
                  $in += pow($base, $pad_up);
              }
          }
    
          $out = "";
          for ($t = floor(log($in, $base)); $t >= 0; $t--) {
              $bcp = bcpow($base, $t);
              $a   = floor($in / $bcp) % $base;
              $out = $out . substr($index, $a, 1);
              $in  = $in - ($a * $bcp);
          }
          $out = strrev($out); // reverse
      }
    
      return $out;
    }


	/**
	 * @param int $num numeric value to be converted to string
	 * 
	 * @return string random (not really) string
	 */
    const SCRAMBLE_FACTOR   = 9876543219;
    const SCRAMBLE_SALT     = 'L9sr0eIu';
    public static function scramble($num) {
        $num *= self::SCRAMBLE_FACTOR;
        return self::alphaID($num, false, 1, self::SCRAMBLE_SALT);
    }


    public static function decodeHtmlEnt($str) {
        $ret = html_entity_decode($str, ENT_COMPAT, 'UTF-8');
        $p2 = -1;
        for(;;) {
            $p = strpos($ret, '&#', $p2+1);
            if ($p === FALSE)
                break;
            $p2 = strpos($ret, ';', $p);
            if ($p2 === FALSE)
                break;
               
            if (substr($ret, $p+2, 1) == 'x')
                $char = hexdec(substr($ret, $p+3, $p2-$p-3));
            else
                $char = intval(substr($ret, $p+2, $p2-$p-2));
               
            //echo "$char\n";
            $newchar = iconv(
                'UCS-4', 'UTF-8',
                chr(($char>>24)&0xFF).chr(($char>>16)&0xFF).chr(($char>>8)&0xFF).chr($char&0xFF)
            );
            //echo "$newchar<$p<$p2<<\n";
            $ret = substr_replace($ret, $newchar, $p, 1+$p2-$p);
            $p2 = $p + strlen($newchar);
        }
        return $ret;
    }


    /**
	 * @param string $string to encrypt
     * 
	 * 
	 * @return int original, decoded number / id
	 */
    public static function encrypt($string, $key = null, $method = null) {
        if(!isset($key)) {
            $key = self::DEFAULT_ENCRYPT_SALT;
        }
        if(!isset($method)) {
            $method = 'aes-128-ctr';
        }
        $ivlen = openssl_cipher_iv_length($method);
        $iv     = substr(self::OPEN_SSL_CONSTANT_IV,0, $ivlen);

        return openssl_encrypt($string, $method, $key, null, $iv);
        // return openssl_encrypt($string, $method, $key, null, null);
    }

    /**
	 * @param string $string to encrypt
     * 
	 * 
	 * @return int original, decoded number / id
	 */
    public static function decrypt($string, $key = null, $method = null) {
        if(!isset($key)) {
            $key = self::DEFAULT_ENCRYPT_SALT;
        }
        if(!isset($method)) {
            $method = 'aes-128-ctr';
        }

        $ivlen = openssl_cipher_iv_length($method);
        $iv     = substr(self::OPEN_SSL_CONSTANT_IV,0, $ivlen);

        return openssl_decrypt($string, $method, $key, null, $iv);
    }
    

    public static function nl2p($string, $line_breaks = true, $xml = true) {

        $string = str_replace(array('<p>', '</p>', '<br>', '<br />'), '', $string);
        
        // It is conceivable that people might still want single line-breaks
        // without breaking into a new paragraph.
        if ($line_breaks == true)
            return '<p>'.preg_replace(array("/([\n]{2,})/i", "/([^>])\n([^<])/i"), array("</p>\n<p>", '$1<br'.($xml == true ? ' /' : '').'>$2'), trim($string)).'</p>';
        else 
            return '<p>'.preg_replace(
            array("/([\n]{2,})/i", "/([\r\n]{3,})/i","/([^>])\n([^<])/i"),
            array("</p>\n<p>", "</p>\n<p>", '$1<br'.($xml == true ? ' /' : '').'>$2'),
        
            trim($string)).'</p>'; 
    }



	/**
	 * @param string $num numeric value to be converted to string
	 * 
	 * @return int original, decoded number / id
	 */
    public static function unscramble($string) {
        $num = self::alphaID($string, true, 1, self::SCRAMBLE_SALT);
        return $num/self::SCRAMBLE_FACTOR;
    }


	/**
     * Make alias from a string: remove special chars and replace spaces with dashes
	 * 
     * @param string $str
	 * @return string alias
	 */
    public static function make_alias($str) {
        $str = preg_replace("/([^0-9a-zA-Zא-ת +]+)/", '', $str);

        //Plus:
        $str = preg_replace("/[+]+/", '+', $str);
        $str = str_replace('+', '-plus-', $str);

        //Spaces and dashes
        $str = str_replace(' ', '-', $str);
        $str = preg_replace("/-+/", '-', trim($str,'-'));


        return $str;
    }


    public static function smart_trim($text, $max_len, $trim_middle = false, $trim_chars = '...') {
        $text = trim($text);
    
        if (strlen($text) < $max_len) {
    
            return $text;
    
        } elseif ($trim_middle) {
    
            $hasSpace = strpos($text, ' ');
            if (!$hasSpace) {
                //The entire string is one word. Just take a piece of the  beginning and a piece of the end.
                $first_half = substr($text, 0, $max_len / 2);
                $last_half = substr($text, -($max_len - strlen($first_half)));
            } else {
                // Get last half first as it makes it more likely for the first
                // half to be of greater length. This is done because usually the
                // first half of a string is more recognizable. The last half can
                // be at most half of the maximum length and is potentially
                // shorter (only the last word).
    
                $last_half = substr($text, -($max_len / 2));
                $last_half = trim($last_half);
                $last_space = strrpos($last_half, ' ');
                if (!($last_space === false)) {
                    $last_half = substr($last_half, $last_space + 1);
                }
                $first_half = substr($text, 0, $max_len - strlen($last_half));
                $first_half = trim($first_half);
                if (substr($text, $max_len - strlen($last_half), 1) == ' ') {
                    // The first half of the string was chopped at a space.
                    $first_space = $max_len - strlen($last_half);
                } else {
                    $first_space = strrpos($first_half, ' ');
                }
                if (!($first_space === false)) {
                    $first_half = substr($text, 0, $first_space);
                }
            }
    
            return $first_half.$trim_chars.$last_half;
    
        } else {
    
            $trimmed_text = substr($text, 0, $max_len);
            $trimmed_text = trim($trimmed_text);
            if (substr($text, $max_len, 1) == ' ') {
                // The string was chopped at a space.
    
                $last_space = $max_len;
            } else {
                //In PHP5, we can use 'offset' here -Mike
    
                $last_space = strrpos($trimmed_text, ' ');
            }
    
            if (!($last_space === false)) {
                $trimmed_text = substr($trimmed_text, 0, $last_space);
            }
            return self::remove_trailing_punctuation($trimmed_text).$trim_chars;
    
        }
    
    }

    
    // Strip trailing punctuation from a line of text.
    // @param  string $text The text to have trailing punctuation removed from.
    // @return string       The line of text with trailing punctuation removed.
    public static function remove_trailing_punctuation($text) {
        return preg_replace("'[^a-zA-Z_0-9א-ת]+$'s", '', $text);
    }
    
}